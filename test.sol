contract SimpleStorage {
    byte32 storedData;

    function set(byte32 x) {
        storedData = x;
    }

    function get() constant returns (byte32) {
        return storedData;
    }
}