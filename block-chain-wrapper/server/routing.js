var express = require('express'),
    compression = require('compression'),
    helmet = require('helmet'),
    bodyParser = require('body-parser'),
    jwt = require('express-jwt');

var logger = require('./logger');
var config = require('./config').getParam();

var lib = {
    message: require('./module/returnmessage'),
    config: config,
    logger: logger
};

console.log(__dirname)
var app = express();

app.use(compression());
app.use(helmet());
app.use(bodyParser.json({ limit: '100mb', strict: false }));
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/' + config.APPPATH, express.static('../app', { index: 'index.html' }));


app.use(function (error, req, res, next) {
    //Catch json error
    if (error) {
        res.json(lib.message.error("error data"));
    } else {
        next();
    }
});

app.use(function (req, res, next) {
    res.setHeader("Access", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
    res.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1.
    res.setHeader("Pragma", "no-cache"); // HTTP 1.0.
    res.setHeader("Expires", "0"); // Proxies.
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST,OPTION');
    res.setHeader("Access-Control-Allow-Headers", "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    next();
});

app.disable('x-powered-by');

app.set('port', process.env.PORT || config.PORT);

app.post('/' + config.APPPATH + '/public/:service', function (req, res) {
    try {
        var service = require('./public/' + req.params.service);
        var opts = {};
        service.post(opts, function (err, result) {
            if (err) {
                lib.logger.request.warn(err);
                res.status(200);
                res.json(lib.message.fail(err));
            } else {
                res.status(200);
                res.json(lib.message.success(result));
            }
        }, req, lib);
    }
    catch (e) {
        catchErrorHandling(e)
    }
});



app.post('/' + config.APPPATH + '/:service', jwt({
    secret: lib.config.SECRET,
    getToken: function fromHeaderOrQuerystring(req) {
        console.log(req.params.service)
        if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
            return req.headers.authorization.split(' ')[1];
        } else if (req.query && req.query.token) {
            return req.query.token;
        }
        return null;
    }
}), function (req, res) {
    try {
        var service = require('./service/' + req.params.service);
        service.post(function (err, result) {
            if (err) {
                res.status(200);
                lib.logger.request.warn(err);
                res.json(lib.message.fail(err));
            } else {
                res.status(200);
                res.json(lib.message.success(result));
            }
        }, req, lib);
    }
    catch (e) {
        catchErrorHandling(e)
    }
});


app.get('*', function (req, res, next) {
    var err = new Error();
    err.status = 404;
    next(err);
});

app.use(function (err, req, res, next) {
    if (err.status !== 404) {
        return next();
    }
    res.send(err.message || '** 404 Not Found  Щ(ಠ益ಠЩ)**');
});

process.on('uncaughtException', function (err) {
    console.log(err.stack);
    lib.logger.fatal.fatal(err.stack);
});

process.on('exit', function () {
    connection.close();
    console.log("Application Abort");
});

function catchErrorHandling(e) {

    console.log(e);
    lib.logger.request.fatal(e.stack);
    if (e instanceof TypeError) {
        res.status(404);
        res.json(lib.message.error("Not found"));
    } else {
        res.status(500);
        res.json(lib.message.error(e.message));
    }
}

exports.app = app;
