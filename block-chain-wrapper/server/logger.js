var bunyan = require('bunyan');

exports.debug = bunyan.createLogger({
    name: "debug",
    streams: [{
        type: 'rotating-file',
        path: './logs/debug.log',
        period: '1d',   // daily rotation
        count: 180        // keep 3 back copies
    }]
});

exports.fatal = bunyan.createLogger({
    name: "fatal",
    streams: [{
        path: './logs/code_error.log'
    }]
});