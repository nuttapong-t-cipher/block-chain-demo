var http = require('http'),
    https = require('https'),
    fs = require('fs');

var config = require('./config').getParam();
var routing = require('./routing');

/*var serverOption = {
    key: fs.readFileSync(config.PRIVATE_KEY_PATH, 'utf8'),
    cert: fs.readFileSync(config.CERT_PATH, 'utf8'),
    ca: fs.readFileSync(config.CA_PATH, 'utf8'),
};*/


var server = http.createServer(routing.app);
server.timeout = config.HTTPREQUESTTIMEOUT;
server.listen(routing.app.get('port'), function () {
    console.log('Express server listening on port ' + routing.app.get('port'));
});

