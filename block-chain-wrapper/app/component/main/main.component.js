angular.module('main', [

]).controller('MainController', function ($scope) {

    $scope.coinbase = web3.eth.coinbase;
    $scope.accounts = web3.eth.accounts;


    $scope.unlock = function(account){
        var password = prompt("Please enter account password");
        try {
            web3.personal.unlockAccount(account, password);
            alert("Unlock " + account + " Success.");
        } catch (error) {
            alert(error);
        }
    }
}); 