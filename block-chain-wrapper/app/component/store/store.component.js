angular.module('store', [

]).controller('StoreController', function ($scope) {
    var contract_abi = JSON.parse(localStorage.getItem("contract_abi"));
    var contract_addr = JSON.parse(localStorage.getItem("contract_addr"));
    console.log(contract_addr)
    console.log(contract_abi)
    var contract = web3.eth.contract(contract_abi).at(contract_addr);
    console.log(contract)

    var newFile = document.getElementById('files');
    newFile.onchange = function (evt) {
        parseAndRenderPDF(evt, this);
    }

    var parseAndRenderPDF = function (evt, that) {
        var canvas = document.getElementById('canvas');
        var tgt = evt.target || window.event.srcElement;
        var files = tgt.files;
        // FileReader support
        if (FileReader && files && files.length) {
            var fr = new FileReader();
            var extension = files[0].name.split('.').pop().toLowerCase();
            //if extension is not pdf ,then stop processing further and exit.
            if (extension !== 'pdf') {
                alert('please choose a PDF file');
                return;
            } else {
                //parse and process the PDF file.
                fr.onload = function (e) {
                    console.debug("Loaded the PDF file");
                    console.log(e)
                    $scope.data = e.target.result;
                    $scope.$apply();
                }
                fr.readAsDataURL(that.files[0]);
            }
        }
    }

    $scope.store = function(data){
        console.log(contract)
        contract.set(data.toString(),{from:web3.eth.accounts[0],gas:4712388})
    }

    $scope.retrive = function () {
        console.log(contract)
        console.log(contract.get({ from: web3.eth.accounts[0] }))
        var store_data = contract.get();
        console.log(store_data)
        $scope.data = web3.toAscii(store_data);

       /* var dlnk = document.getElementById('dwnldLnk');
        dlnk.href = $scope.data;

        dlnk.click();*/
    }

}); 