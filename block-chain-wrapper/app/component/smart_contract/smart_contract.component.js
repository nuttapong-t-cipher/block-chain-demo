angular.module('sa', [

]).controller('SmartContractController', function ($scope) {

    $scope.contract = localStorage.getItem("solidity_code") || "";

    $scope.deploy = function () {
        var contract_source = angular.copy($scope.contract)

        localStorage.setItem("solidity_code", contract_source);

        var contract = web3.eth.compile.solidity(contract_source)

        console.log(contract)
        console.log(compile_object)
        var compile_object = contract[Object.keys(contract)[0]]

        var code = compile_object.code;

        console.log(code)

        var abi = compile_object.info.abiDefinition;

        console.log(abi)

        localStorage.setItem("contract_abi", JSON.stringify(abi));
        var myContract;


        web3.eth.defaultAccount = web3.eth.coinbase;

        try {
            web3.eth.contract(abi).new({ data: code, gas: 4712388 }, function (err, contract) {
                if (err) {
                    console.error(err);
                    return;
                    // callback fires twice, we only want the second call when the contract is deployed
                } else if (contract.address) {
                    myContract = contract;
                    console.log('address: ' + myContract.address);
                    localStorage.setItem("contract_addr", JSON.stringify(myContract.address));
                    
                    //0xd54db6450cffa479c04d441c8bc2161e596fac4e
                    $scope.status = 'Mined!';

                }
            });
        } catch (error) {
            alert(error);
        }

    }

    $scope.estimateGas = function(){
        var contract_source = angular.copy($scope.contract)
        $scope.cost = web3.eth.estimateGas(contract_source)
    }

}); 