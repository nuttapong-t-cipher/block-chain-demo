angular.module('routing', [
    'ui.router',
    'oc.lazyLoad'
]).config(function router($stateProvider, $urlRouterProvider) {

    var base_component_path = "component/";
    $stateProvider.
        state('app', {
            url: "/",
            abstract:true,
            templateUrl: base_component_path + 'nav/nav.component.html',
            controller: 'NavController',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(base_component_path + 'nav/nav.component.js');
                }]
            }
        }).
        state('app.main',{
            url:"main",
            controller: 'MainController',
            templateUrl: base_component_path + 'main/main.component.html',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(base_component_path + 'main/main.component.js');
                }]
            }
        }).
        state('app.smartcontract', {
            url: "smartcontract",
            controller: 'SmartContractController',
            templateUrl: base_component_path + 'smart_contract/smart_contract.component.html',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(base_component_path + 'smart_contract/smart_contract.component.js');
                }]
            }
        }).
        state('app.store', {
            url: "store",
            controller: 'StoreController',
            templateUrl: base_component_path + 'store/store.component.html',
            resolve: {
                loadMyCtrl: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load(base_component_path + 'store/store.component.js');
                }]
            }
        });;

    $urlRouterProvider.otherwise('/main');

});